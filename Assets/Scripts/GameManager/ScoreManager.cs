﻿using System;
using TMPro;
using  UnityEngine;

namespace GameManager
{
  public class ScoreManager : MonoSingleton<ScoreManager>
  {
    [SerializeField ] private TextMeshProUGUI scoreText;
    [SerializeField ] private TextMeshProUGUI finalScoreText;

    private GameManager gameManager;
    private int plyerScore;
    public void Init(GameManager gameManager)
    {
      this.gameManager = gameManager;
      this.gameManager.OnRestarted += OnRestarted;
      HideScore(false);
      SetScore(0);
    }

    public void SetScore(int score)
    {
      scoreText.text = $"Score : {score}";
      plyerScore = score;
    }

    private void Awake()
    {
     Debug.Assert(scoreText != null, "scoreText cannot null"); 
    }

    private void OnRestarted()
    {
      finalScoreText.text = $"Player Score : {plyerScore}";
      gameManager.OnRestarted -= OnRestarted;
      HideScore(true);
      SetScore(0);
    }

    private void HideScore(bool hide)
    {
      scoreText.gameObject.SetActive(!hide);
    }
  }
}


