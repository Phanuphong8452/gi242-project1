﻿using System;
using SpaceShip;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace GameManager
{
    public class GameManager : MonoSingleton<GameManager>
    {
        [SerializeField]private Button startButton;
        [SerializeField]private Button restartButton;
        [SerializeField]private Button quitButton;
        [SerializeField]private Button stageTwoButton;
        [SerializeField]private RectTransform endScene;
        [SerializeField]private RectTransform dialog;
        [SerializeField]private RectTransform winMessage;
        [SerializeField]private RectTransform loseMessage; 
        [SerializeField]private PlayerSpaceShip playerSpaceship;
        [SerializeField]private EnemySpaceShip enemySpaceShip;
        [SerializeField]private ScoreManager scoreManager;

        public event Action OnRestarted;
        
        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemySpaceshipMoveSpeed;

        private void Awake()
        {
           Debug.Assert(startButton!= null,"startButton cannot be null");
           Debug.Assert(dialog!= null,"dialog cannot be null");
           Debug.Assert(playerSpaceship!= null,"playerSpaceship cannot be null");
           Debug.Assert(enemySpaceShip!= null,"enemySpaceship cannot be null");
           Debug.Assert(scoreManager!= null,"scoreManager cannot be null");
           Debug.Assert(playerSpaceshipHp > 0,"playerSpaceship hp has to be more than zero");
           Debug.Assert(playerSpaceshipMoveSpeed > 0,"playerSpaceshipMoveSpeed has to be more than zero");
           Debug.Assert(enemySpaceshipHp > 0,"enemySpaceshipHp has to be more than zero");
           Debug.Assert(enemySpaceshipMoveSpeed > 0,"enemySpaceshipMoveSpeed has to be more than zero");
           Debug.Assert(restartButton != null,"restartButton cannot be null");
           Debug.Assert(endScene!= null,"endscene cannot be null");
           Debug.Assert(quitButton!= null,"quit cannot be null");
           Debug.Assert(stageTwoButton != null,"stageTwoButton cannot be null");
           Debug.Assert(winMessage!= null,"winMessage!= null");
           Debug.Assert(loseMessage!= null,"loseMessage!= null");

           startButton.onClick.AddListener(OnStartButtonClicked);
           restartButton.onClick.AddListener(OnRestartButtonclick);
           quitButton.onClick.AddListener(OnQuitButtonClick);
           stageTwoButton.onClick.AddListener(OnStageTwoButtonClick);
           
           
        }

        private void OnStageTwoButtonClick()
        {
            SceneManager.LoadScene("SecondStage");
        }

        private void OnStartButtonClicked()
        {
            dialog.gameObject.SetActive(false);
            StartGame();
        }

        private void StartGame()
        {
            scoreManager.Init(this);
            SpawnPlayerSpaceShip();
            SpawnEnermySpaceShip();
        }

        private void SpawnPlayerSpaceShip()
        {
            var spaceship = Instantiate(playerSpaceship);
            spaceship.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spaceship.OnExploded += OnPlayerSpaceshipExploded;
        }

        private void OnPlayerSpaceshipExploded()
        {
            Restart();
            winMessage.gameObject.SetActive(false);
            loseMessage.gameObject.SetActive(true);
            stageTwoButton.gameObject.SetActive(false);
        }

        private void OnRestartButtonclick()
        {
            SceneManager.LoadScene("Game");
        }

        private void SpawnEnermySpaceShip()
        {
            var spaceship = Instantiate(enemySpaceShip);
            spaceship.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
            spaceship.OnExploded += OnEnemySpaceshipExploded;
        }

        private void OnEnemySpaceshipExploded()
        {
            scoreManager.SetScore(1);
            endScene.gameObject.SetActive(true);
        }

        private void Restart()
        {
            DestroyRemainingShip();
            endScene.gameObject.SetActive(true);
            OnRestarted?.Invoke();
        }

        private void DestroyRemainingShip()
        {
            var remainingEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in remainingEnemies)
            {
                Destroy(enemy);
            }
            
            var remainingPlayer = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in remainingPlayer)
            {
                Destroy(player);
            }
        }
        private void OnQuitButtonClick()
        {
            Application.Quit();
        }
    }
}
