﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace SpaceShip
{
    public class PlayerSpaceShip : BaseSpaceShip, IDamagable
    {
        public event Action OnExploded;

        [SerializeField] private AudioClip playerFireSound;
        [SerializeField] private float playerFireSoundVolume = 0.3f;
        [SerializeField] private AudioClip playerExplode;
        [SerializeField] private float playerExplodeSoundVolume = 0.5f;

        private void Awake()
        {
           Debug.Assert(defaultBullet != null, "defaultBullet cannot be null"); 
           Debug.Assert(gunPosition != null, "gunPosition cannot be null");
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public override void Fire()
        {
            AudioSource.PlayClipAtPoint(playerFireSound,Camera.main.transform.position,playerFireSoundVolume);
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Init(Vector2.up);
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }

        public void Explode()
        {
            AudioSource.PlayClipAtPoint(playerExplode,Camera.main.transform.position,playerExplodeSoundVolume);
           Debug.Assert(Hp <= 0, "HP is more than zero");
           Destroy(gameObject);
           OnExploded?.Invoke();
        }
    }
}
