﻿using System;
using System.Collections;
using System.Collections.Generic;
using SpaceShip;
using UnityEngine;
using UnityEngine.Serialization;

namespace EnemyShip
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private EnemySpaceShip enemySpaceShip;
        [SerializeField] private float chasingThresholdDistance;

        private void Update()
        {
            MoveToPlayer();
            enemySpaceShip.Fire();
        }

        private void MoveToPlayer()
        {
            //TODO : Implement this late
        }
    }
}